
import java.io.ByteArrayOutputStream
import kotlin.script.experimental.jvm.util.KotlinJars
import org.gradle.jvm.tasks.Jar


/*
 * This file was generated by the Gradle 'init' task.
 *
 * This generated file contains a sample Kotlin library project to get you started.
 * For more details on building Java & JVM projects, please refer to https://docs.gradle.org/8.7/userguide/building_java_projects.html in the Gradle documentation.
 */

plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    alias(libs.plugins.jvm)
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.23"
    // Apply the java-library plugin for API and implementation separation.
    `java-library`

    id("com.github.gmazzo.buildconfig") version "5.3.5"
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    //implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.7.1")
    implementation ("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
    implementation ("io.ktor:ktor-serialization-kotlinx-json:2.3.10")
    implementation ("io.ktor:ktor-client-android:2.3.10")
    implementation ("io.ktor:ktor-client-content-negotiation:2.3.10")
    implementation ("io.ktor:ktor-client-logging:2.3.10")
    implementation ("org.slf4j:slf4j-simple:2.0.13")

    // Use the Kotlin JUnit 5 integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")

    // Use the JUnit 5 integration.
    testImplementation(libs.junit.jupiter.engine)

    testRuntimeOnly("org.junit.platform:junit-platform-launcher")

    // This dependency is exported to consumers, that is to say found on their compile classpath.
    api(libs.commons.math3)

    // This dependency is used internally, and not exposed to consumers on their own compile classpath.
    implementation(libs.guava)
    implementation(kotlin("reflect"))
}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
}

val h: String = ByteArrayOutputStream().use { outputStream ->
    project.exec {
        executable("git")
        //args("log", "--oneline", "-1", "--format=format:%h", ".")
        args("rev-parse", "HEAD")
        standardOutput = outputStream
    }
    outputStream.toString().trim()
}
val t: String = ByteArrayOutputStream().use { outputStream ->
    project.exec {
        executable("git")
        //args("log", "--oneline", "-1", "--format=format:%h", ".")
        args("log", "-1","--format=%ai")
        standardOutput = outputStream
    }
    outputStream.toString().trim()
}

buildConfig {
    buildConfigField("LAST_GIT_COMMIT_HASH", h)
    buildConfigField("LAST_GIT_COMMIT_TIME", t)
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
    //forkEvery=1
    systemProperties["fetchBaseUrl"] = providers.gradleProperty("fetchBaseUrl").get()
    systemProperties["fetchPath"] = providers.gradleProperty("fetchPath").get()
}

val fatJar = task("fatJar", type = Jar::class) {
    archiveBaseName = "${project.name}-fat"
    manifest {
        attributes["Implementation-Title"] = "Gradle Jar File Example"
        attributes["Implementation-Version"] = version
        //attributes["Main-Class"] = "com.mkyong.DateUtils"
    }
    from(configurations.runtimeClasspath.get().map{ if (it.isDirectory) it else zipTree(it) })
    with(tasks.jar.get() as CopySpec)
    duplicatesStrategy=DuplicatesStrategy.EXCLUDE
}

tasks.named<DefaultTask>("jar"){
    dependsOn(fatJar)
}