/*
 * This source file was generated by the Gradle 'init' task
 */
package org.example

import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertTrue


class LibraryTest {
    @Test
    fun someLibraryMethodReturnsTrue() {
        val resp = runBlocking {
            val r = Library.fetchHash(
                System.getProperty("fetchBaseUrl"),
                System.getProperty("fetchPath"),
                //hashMapOf(("hash" to "1234567890")),
                mapOf(),
                debug = true,
            )
            when (r) {
                is FetchResponse.Success<*> -> {
                    if (r.resp is SubmoduleCommitHash) {
                        println(r.resp)
                    } else {
                        println(r)
                    }
                }

                is FetchResponse.Error -> {
                    println("message: ${r.message}")
                    println("exception: ${r.exception}")
                }

                is FetchResponse.HtmlError -> {
                    println(r.message)
                    println(r.body)
                    println(r.code)
                }
            }
            return@runBlocking r
        }
        assert((resp is FetchResponse.Success<*>).and((resp as FetchResponse.Success<*>).resp is SubmoduleCommitHash))
    }
}
